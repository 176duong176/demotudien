package Word;

import java.util.ArrayList;

public class Dictionary {

    private static final ArrayList<Word> dictionary = new ArrayList<>();
    public static int index_start;

    public Dictionary() {
    }

    public ArrayList<Word> getDict() {
        return dictionary;
    }

    public void push(Word vocabulary) {
            dictionary.add(vocabulary);
    }

    public static Word binaryLookup(int min, int max, String vocabulary) {
        if (min >= max) {
            Word voca = dictionary.get(min);
            if (voca.geteng().startsWith(vocabulary)) {
                index_start = min;
                return dictionary.get(min);
            }
            return null;
        }

        int midle = (min + max) / 2;
        Word voca = dictionary.get(midle);
        String currentVocabulary = voca.geteng();
        int compare = currentVocabulary.compareTo(vocabulary);
        if (compare == 0) {
            index_start = midle;
            return voca;
        }
        if (compare > 0) {
            return binaryLookup(min, midle, vocabulary);
        }
        return binaryLookup(midle + 1, max, vocabulary);
    }

    public void search(String eng, ArrayList<Word> ls, Dictionary dictionary) {
        //history
        Word kq = binaryLookup(1, dictionary.getDict().size() - 1, eng);
        if (kq == null) {
            System.out.println("Không tìm thấy từ.");
        } else {
            System.out.println("|" + kq.geteng() + "|");
            System.out.println(kq.getvn());
            ls.add(kq);
        }
    }

    public static int getStartWordIndex() {
        return index_start;
    }
}
