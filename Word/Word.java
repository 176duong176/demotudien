package Word;


public class Word {
    private final String eng;
    private String vn;

    public Word() {
        this("", "");
    }

    /**
     * 
     *
     * @param eng
     * @param vn
     */
    public Word(String eng, String vn) {
        this.eng = eng;
        this.vn = vn;
    }

    public String geteng() {
        return eng;
    }

    public String getvn() {
        return vn;
    }

    public void setvn(String vn) {
        this.vn = vn;
    }

    public void print() {
        System.out.printf("| %-20s | %s\n", eng, vn);
    }

}
