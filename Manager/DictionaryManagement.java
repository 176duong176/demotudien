package Manager;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import Word.Dictionary;
import Word.Word;




public class DictionaryManagement {
    /**
     * Đọc file dictionary.txt
     */
    public static void readFile(String fileName, Dictionary dictionary) {
        try {
            File f = new File(fileName);
            insertFromFile(f, dictionary);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * xóa từ
     */
    public static void deleteWord(Dictionary dictionary) throws IOException{
        String eng = Validation.string_in("Nhập từ muốn xóa: ", ".+");
        Word tim = dictionaryLookup(eng, dictionary);
        if (tim == null) {
            System.out.println("Không tìm thấy kết quả nào giống.");
        } else {
            dictionary.getDict().remove(tim);
        }
    }

    public static void insertFromFile(File file, Dictionary dictionary) throws IOException {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while ((line = br.readLine()) != null) {
            try {
                String[] vocabularys = line.split("\\t+");
                String result = "";
                for (int i = 1; i < vocabularys.length; i++) {
                    result += vocabularys[i] + "\n";
                }
                dictionary.push(new Word(vocabularys[0], result));
            } catch (IndexOutOfBoundsException e) {
                System.out.print("");
            }
        }
        br.close();
        fr.close();
    }

    /**
     * thêm/ sửa từ
     */
    public static void add_exit(Dictionary dictionary) throws IOException {
        String eng = Validation.string_in("Nhập từ tiếng anh: ", ".+");
        Word tim = dictionaryLookup(eng, dictionary);
        if (tim == null) {
            String vn = Validation.string_in("Nhập nghĩa tiếng việt: ", ".+");
            dictionary.push(new Word(eng, vn));
            DictionaryCommandline.ls.add(new Word(eng, vn));

        } else {
            if (Validation.Y_N("Cập nhật từ ? (Y/N): ")) {
                String vn = Validation.string_in("Xác nhận nghĩa mới: ", ".+");
                tim.setvn(vn);
            }
            dictionary.push(tim);
            DictionaryCommandline.ls.add(tim);
        }
    }


    /**
     * Tra từ
     */
    public static Word dictionaryLookup(String eng, Dictionary dictionary) {
        for (Word vocabulary : dictionary.getDict()) {
            if (vocabulary.geteng().equalsIgnoreCase(eng)) {
                return vocabulary;
            }
        }
        return null;
    }

}
