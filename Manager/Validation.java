package Manager;

import java.util.Scanner;

/**
 * Kiểm tra đầu vào của hàm main
 */
public class Validation {
    private static final Scanner sc = new Scanner(System.in);

    public static int inputIntLimit(String input, int min, int max) {
        System.out.print(input);
        while (true) {
            try {
                int kq = Integer.parseInt(sc.nextLine().trim());
                if (kq < min || kq > max) {
                    throw new NumberFormatException();
                }
                return kq;
            } catch (NumberFormatException e) {
                System.out.println("Chọn số trong khoảng từ " + min + " đến " + max + ".");
                System.out.print("Số vừa nhập của bạn không hợp thể. Nhập lại trong khoảng [" + min + ";" 
                                    + max + "]");
            }
        }
    }

    public static String string_in(String input, String regex) {
        while (true) {
            System.out.print(input);
            String kq = sc.nextLine().trim();
            if (!kq.matches(regex)) {
                System.out.println("Nhập dữ liệu.");
            } else {
                return kq;
            }
        }
    }

    public static boolean Y_N(String prompt) {
        while (true) {
            String kq = string_in(prompt, ".+");

            if (kq.equalsIgnoreCase("y")) {
                return true;
            }
            if (kq.equalsIgnoreCase("n")) {
                return false;
            }
            System.out.println("\nChọn y/Y hoặc n/N.");
            System.out.print("Nhập lại: ");
        }
    }
}
