package Manager;

import java.io.IOException;
import java.util.ArrayList;

import Word.Dictionary;
import Word.Word;

public class DictionaryCommandline {

    public static final ArrayList<Word> ls = new ArrayList<>();

    public static void showAllWords(Dictionary dictionary) {
        System.out.println("STT || English          || Vietnamese          ||");
        for (int i = 0; i < dictionary.getDict().size(); i++) {
            System.out.print(i + 1 + "   ");
            dictionary.getDict().get(i).print();
        }
    }

    public static void dictionaryAdvanced(Dictionary dictionary) throws IOException {
        DictionaryManagement.readFile("src\\dictionary.txt", dictionary);
        while (true) {
            System.out.println("1. Xem từ điển.");
            System.out.println("2. Xóa từ.");
            System.out.println("3. Tra từ.");
            System.out.println("4. Sửa.");
            System.out.println("5. Thoát.");
            int number_chon = Validation.inputIntLimit("Chọn thao tác: ", 1, 5);
            switch (number_chon) {
                case 1:
                    showAllWords(dictionary);
                    break;
                case 2:
                    DictionaryManagement.deleteWord(dictionary);
                    break;
                case 3:
                    String eng = Validation.string_in("Nhập từ cần dịch: ", ".+");
                    dictionary.search(eng, ls, dictionary);
                    break;
                case 4:
                    DictionaryManagement.add_exit(dictionary);
                    break;
                case 5:
                    return;
            }
        }
    }
}
