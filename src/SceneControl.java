import Manager.DictionaryCommandline;
import Manager.DictionaryManagement;
import Word.Dictionary;
import Word.Word;

import java.io.*;
import java.util.Set;

import java.util.ArrayList;
import java.util.HashSet;

import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javazoom.jl.decoder.JavaLayerException;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

public class SceneControl {
    private final Font size_font_max = Font.font("Arial", 14);
    private final Font size_font_min = Font.font("Arial", 12);
    private final Cursor cursor_img = Cursor.MOVE;
    private final int size_button = 14;
    final String style_gd = "giao dien";
    public String HUY = "huy";
    public String OK = "xac nhan";
    final double Meaning = 70.0;

    private Dictionary dictionary = new Dictionary();
    private ArrayList<Word> diction = dictionary.getDict();

    @FXML
    public AnchorPane announcementAnchorPane;
    @FXML
    public Label announcementLabel;

    @FXML
    private TextField searchTextField;
    @FXML
    private Button searchButton;
    private Button advanceSearch;

    @FXML
    private TabPane wordTabPane;
    private Set<Tab> tabsToDelete = new HashSet<Tab>();
    @FXML
    private GridPane wordsGridPane;
    private Set<Node> nodesToDelete = new HashSet<Node>();
    @FXML
    private AnchorPane translationField;

    @FXML
    private GridPane historyGridPane;
    private Set<Node> historyNodes = new HashSet<Node>();
    private HashSet<String> historyWord = new HashSet<String>();
    private int historyCount = 0;

    @FXML
    public void initialize() throws IOException, JavaLayerException {
        DictionaryManagement.readFile("src\\dictionary.txt", dictionary);

        
    }

    
    @FXML
    //tìm kiếm từ trong từ điển theo binary
    public void search() {
        announcementLabel.setText("");
        String timkiem = searchTextField.getText();

        if (timkiem == "") {
            return;
        }
        resetTranslationField();
        resetTabPane();
        announcementLabel.setText("Loading...");

        defaultSearch(timkiem);
    }

    public void defaultSearch(String timkiem) {
        Word index_start = Dictionary.binaryLookup(0, diction.size(), timkiem);
        if (index_start == null) {
            announcementLabel.setText("Không tìm thấy từ.");
        } else {
            int tab_Count = 1;

            Tab currentTab = new Tab("Trang " + tab_Count);
            ScrollPane currentScroll = new ScrollPane();
            GridPane currentGrid = new GridPane();
            addToTabPane(currentTab);
            currentTab.setContent(currentScroll);
            currentScroll.setContent(currentGrid);

            int row = 0;
            int start = Dictionary.getStartWordIndex();
            Word each = diction.get(start);
            String target = each.geteng();
            int tab_limit_vocaburary = 125; // số từ tối đa trong một tab
            while (target.startsWith(timkiem)) {
                addToGridPane(currentGrid, nodesToDelete, newWordButton(target, each.getvn(), row), row);
                row++;
                start++;
                if (start >= diction.size()) {
                    break;
                }
                each = diction.get(start);
                target = each.geteng();

                if (row >= tab_limit_vocaburary) {
                    row = 0;
                    tab_Count++;
                    currentTab = new Tab("Trang " + tab_Count);
                    currentScroll = new ScrollPane();
                    currentGrid = new GridPane();
                    addToTabPane(currentTab);
                    currentTab.setContent(currentScroll);
                    currentScroll.setContent(currentGrid);
                }
            }


            if (!announcementAnchorPane.getChildren().contains(advanceSearch)) {
                announcementAnchorPane.getChildren().add(advanceSearch);
                advanceSearch.setOnAction(e -> {
                    announcementAnchorPane.getChildren().remove(advanceSearch);
                });
            }
        }
    }


    /**
     * Xem từ điển
     */
    @FXML
    public void showAllWord(ActionEvent event) throws IOException {
        resetToNormal();

        

    }

    /**
     * xóa từ
     */
    @FXML
    public void removeWord(ActionEvent event) {
        resetToNormal();
        searchTextField.setPromptText("Nhập từ cần xóa");
        searchTextField.setOnKeyTyped(e -> {
        });

        announcementLabel.setText("Nhập từ cần xóa khỏi từ điển.");
        Button confirmButton = newSideButton("Xác nhận", OK);

        confirmButton.setOnMouseClicked(e -> {
            String eng = searchTextField.getText();
            Word tim = DictionaryManagement.dictionaryLookup(eng, dictionary);
            if (tim == null) {
                announcementLabel.setText("Từ này không có trong từ điển.");
                return;
            } else {
                diction.remove(tim);
                try {
                    File file = new File("src\\dictionary.txt");
                    List<String> out = Files.lines(file.toPath())
                            .filter(line -> !line.contains(tim.geteng()))
                            .collect(Collectors.toList());
                    Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            announcementLabel.setText("Xóa từ thành công.");
        });


    }

    /**
     * thêm từ
     */
    @FXML
    public void addWord(ActionEvent event) {
        resetToNormal();
        searchTextField.setPromptText("Nhập từ mà bạn muốn thêm vào từ điển");
        searchTextField.setOnKeyTyped(e -> {
        });

        announcementLabel.setText("Nhập từ bạn muốn thêm.");
        Button confirmButton = newSideButton("Xác nhận", OK);
        Button cancelButton = newSideButton("Hủy", HUY);

        confirmButton.setOnMouseClicked(e -> {
            String eng = searchTextField.getText();
            Word tim = DictionaryManagement.dictionaryLookup(eng, dictionary);
            if (tim == null) {
                searchTextField.setText("");
                searchTextField.setPromptText("Nhập nghĩa từ vừa thêm vào");
                announcementLabel.setText("Nhập nghĩa của từ vừa thêm vào.");
                confirmButton.setOnMouseClicked(e2 -> {
                    String vn = searchTextField.getText();
                    dictionary.push(new Word(eng, vn));
                    DictionaryCommandline.ls.add(new Word(eng, vn));
                    announcementAnchorPane.getChildren().remove(confirmButton);
                    announcementAnchorPane.getChildren().remove(cancelButton);
                    resetToNormal();
                    announcementLabel.setText("Thêm từ thành công.");

                    try {
                        String filename = "src\\dictionary.txt";
                        FileWriter fw = new FileWriter(filename, true);
                        fw.write(eng + "\t" + vn + "\n");
                        fw.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                });
            } else {
                announcementLabel.setText("Cập nhật từ này không?");
                confirmButton.setOnMouseClicked(e2 -> {
                    announcementLabel.setText("Nghĩa mới.");
                    searchTextField.setText("");
                    searchTextField.setPromptText("Nhập nghĩa mới của từ");

                    confirmButton.setOnMouseClicked(e3 -> {
                        String new_VN = searchTextField.getText();
                        tim.setvn(new_VN);
                        DictionaryCommandline.ls.add(new Word(eng, new_VN));
                        announcementAnchorPane.getChildren().remove(confirmButton);
                        announcementAnchorPane.getChildren().remove(cancelButton);
                        resetToNormal();
                        announcementLabel.setText("Chỉnh sửa từ thành công.");

                        try {
                            File file = new File("src\\dictionary.txt");
                            List<String> out = Files.lines(file.toPath())
                                    .filter(line -> !line.contains(tim.geteng()))
                                    .collect(Collectors.toList());
                            Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

                            String filename = "src\\dictionary.txt";
                            FileWriter fw = new FileWriter(filename, true);
                            fw.write(eng + "\t" + new_VN + "\n");
                            fw.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    });
                });
            }
        });

    }


    /**
     * Xóa lịch sử tra từ
     */
    @FXML
    public void deleteHistory(ActionEvent event) {
        resetToNormal();
        
            historyGridPane.getChildren().removeAll(historyNodes);
            historyNodes.clear();
            historyWord.clear();
            historyCount = 0;
    
        
    }



    /**
     * Thoát.
     */
    @FXML
    public void exitDict(ActionEvent event) {
        Platform.exit();
    }

    public void resetTabPane() {
        wordTabPane.getTabs().removeAll(tabsToDelete);
        tabsToDelete.clear();
    }

    public void resetTranslationField() {
        translationField.getChildren().clear();
    }

    public void resetAnnouncePane() {
        announcementAnchorPane.getChildren().clear();
        announcementAnchorPane.getChildren().addAll(announcementLabel);
    }

    public void resetToNormal() {
        announcementLabel.setText("Welcome. Chọn thao tác để thực hiện");
        announcementAnchorPane.getChildren().remove(advanceSearch);
        searchTextField.setText(null);
        searchTextField.setPromptText("Nhập từ hoặc cụm từ cần tìm");
        searchTextField.setOnKeyTyped(e -> {
            search();
        });
        resetTabPane();
        resetTranslationField();
        resetAnnouncePane();
    }


    public void addToTabPane(Tab tab_Number) {
        wordTabPane.getTabs().add(tab_Number);
        tabsToDelete.add(tab_Number);
    }

    public void addToGridPane(GridPane gridPane, Set<Node> nodeArray, Node node, int row) {
        gridPane.add(node, 0, row);
        nodeArray.add(node);
    }

    public void addToHistoryPane(Button but) {
        historyGridPane.add(but, 0, historyCount);
        historyNodes.add(but);
        historyCount++;
    }

    ImageView speakerImageView() throws FileNotFoundException {
        FileInputStream img = new FileInputStream("img\\speaker.png");
        Image image = new Image(img);
        ImageView view = new ImageView(image);
        view.setFitHeight(17.0);
        view.setFitWidth(17.0);
        return view;
    }


    Button newWordButton(String eng, String vn, int row) {
        Button newWordButton = new Button(eng);
        newWordButton.setFont(size_font_max);
        newWordButton.setCursor(cursor_img);
        if (row % 2 == 1) {
            newWordButton.setBackground(Background.EMPTY);
        } else {
            newWordButton.setStyle(style_gd);
        }

        newWordButton.setOnMouseClicked(e -> {
            resetTranslationField();

            Button fakeButt = new Button(eng);
            fakeButt.setFont(size_font_max);
            fakeButt.setStyle(style_gd);
            translationField.getChildren().add(fakeButt);
            AnchorPane.setTopAnchor(fakeButt, 4.0);
            AnchorPane.setLeftAnchor(fakeButt, 7.0);

            showTranslated(eng, vn, row);

            if (historyWord.add(eng)) {
                addToHistoryPane(newHistoryButton(eng, vn));
            }
        });

        return newWordButton;
    }

    Button newHistoryButton(String eng, String vn) {
        Button newHistoryButton = new Button(eng);
        newHistoryButton.setFont(size_font_max);
        newHistoryButton.setCursor(cursor_img);
        if (historyCount % 2 == 0) {
            newHistoryButton.setBackground(Background.EMPTY);
        } else {
            newHistoryButton.setStyle(style_gd);
        }

        newHistoryButton.setOnMouseClicked(e -> {
            resetTranslationField();

            Button fakeButt = new Button(eng);
            fakeButt.setFont(size_font_max);
            fakeButt.setStyle(style_gd);
            translationField.getChildren().add(fakeButt);
            AnchorPane.setTopAnchor(fakeButt, 0.0);
            AnchorPane.setLeftAnchor(fakeButt, 7.0);

            showTranslated(eng, vn, 0);
        });

        return newHistoryButton;
    }

    /**
     * Văn bản.
     */
    TextArea newTextArea(String string) {
        TextArea newTextArea = new TextArea(string);
        newTextArea.setFont(size_font_max);
        newTextArea.setStyle(style_gd);
        newTextArea.setWrapText(true);
        newTextArea.setEditable(false);
        newTextArea.autosize();
        return newTextArea;
    }

    /**
     * Hiển thị nghĩa của từ.
     */
    public void showTranslated(String eng, String vn, int row) {
        TextArea vocabulary_vn = newTextArea(vn);

        translationField.getChildren().add(vocabulary_vn);
        AnchorPane.setTopAnchor(vocabulary_vn, 35.0);
        AnchorPane.setRightAnchor(vocabulary_vn, 4.0);
        AnchorPane.setBottomAnchor(vocabulary_vn, Meaning);
        AnchorPane.setLeftAnchor(vocabulary_vn, 4.0);
    }


    /**
     * button sử dụng(các nút)
     */
    Button newSideButton(String name, String position) {
        Button newButton = new Button(name);
        newButton.setFont(size_font_min);
        newButton.setPrefHeight(size_button);
        newButton.setCursor(cursor_img);
        newButton.setStyle(style_gd);
        announcementAnchorPane.getChildren().add(newButton);
        AnchorPane.setBottomAnchor(newButton, 52.0);
        if (position == OK) {
            AnchorPane.setLeftAnchor(newButton, 46.0);
        } else {
            AnchorPane.setRightAnchor(newButton, 46.0);
        }
        return newButton;
    }
}
